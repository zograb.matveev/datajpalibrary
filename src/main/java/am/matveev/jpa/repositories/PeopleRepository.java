package am.matveev.jpa.repositories;

import am.matveev.jpa.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PeopleRepository extends JpaRepository<Person,Integer>{
      Optional<Person> findByFullName(String fullName);
}
