package am.matveev.jpa.repositories;

import am.matveev.jpa.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BooksRepository extends JpaRepository<Book,Integer>{
    List<Book> findByTitleStartingWith(String title);
}
