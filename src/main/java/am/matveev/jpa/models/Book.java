package am.matveev.jpa.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "Book")
@Getter
@Setter
public class Book{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "title")
    @NotEmpty(message = "Поле не должно быть пустым")
    @Size(min = 3,max = 100,message = "Название книги должно быть между 3 и 100 символами")
    private String title;

    @Column(name = "author")
    @NotEmpty(message = "Поле не должно быть пустым")
    @Size(min = 3,max = 100,message = "Имя должно быть между 3 и 100 символами")
    private String author;


    @Column(name = "year")
    @Min(value = 1500,message = "Год написания должен быть не меньше 1500")
    private int year;

    @ManyToOne
    @JoinColumn(name = "person_id",referencedColumnName = "id")
    private Person owner;

    @Column(name = "taken_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date takenAt;

    @Transient
    private boolean expired;

    public Book(){

    }

    public Book(String title, String author,Date takenAt){
        this.title = title;
        this.author = author;
        this.takenAt = takenAt;
    }

    @Override
    public String toString(){
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", takenAt=" + takenAt +
                '}';
    }
}
