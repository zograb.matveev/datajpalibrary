package am.matveev.jpa.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "Person")
@Getter
@Setter
public class Person{

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "fullName")
    @NotEmpty(message = "Поле не должно быть пустым")
    @Size(min = 3,max = 100,message = "ФИО должно быть между 3 и 100 символов")
    private String fullName;

    @Column(name = "year_of_birth")
    @Min(value = 1940,message = "Год рождения должен быть не меньше 1940")
    private int yearOfBirth;

    @OneToMany(mappedBy = "owner",fetch = FetchType.EAGER)
    private List<Book> books;

    public Person(){

    }

    public Person(String fullName, int yearOfBirth){
        this.fullName = fullName;
        this.yearOfBirth = yearOfBirth;
    }

    @Override
    public String toString(){
        return "Person{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", yearOfBirth=" + yearOfBirth +
                '}';
    }
}
